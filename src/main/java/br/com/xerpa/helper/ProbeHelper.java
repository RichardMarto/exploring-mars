package br.com.xerpa.helper;

import br.com.xerpa.rotation.Directions;

import java.util.HashMap;

import java.util.Map;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class ProbeHelper {

    public Map<Character, Integer> wordCounter (String commands) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        map.put(Directions.RIGHT, 0);
        map.put(Directions.LEFT, 0);
        for (String s : commands.split("")) {
            for (Character c : s.toCharArray()) {
                if (!map.containsKey(c)) {
                    map.put(c, 1);
                } else {
                    int count = map.get(c);
                    map.put(c, count + 1);
                }
            }
        }
        return map;
    }

    public final UnaryOperator<String> clean = command -> command.replace("[^R^L^M]", "");

    public final Predicate<String> isValid = position -> position.matches("\\d\\s\\d\\s[NESW]");
}
