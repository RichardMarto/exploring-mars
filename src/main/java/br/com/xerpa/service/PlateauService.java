package br.com.xerpa.service;

import br.com.xerpa.factory.PlateauFactory;
import br.com.xerpa.factory.ProbeFactory;
import br.com.xerpa.model.Plateau;
import br.com.xerpa.model.Probe;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PlateauService {

    private final ProbeFactory probeFactory = new ProbeFactory();
    private Plateau plateau;

    public PlateauService(String maxPoint) {
        plateau = new PlateauFactory().getPlateau(maxPoint);
    }

    BiPredicate<Long, Long> have = (x, y) -> x >= 0 && x <= plateau.getMaxX() && y >= 0 && y<= plateau.getMaxY();

    Predicate<String> contains = x -> {
        Probe probe = probeFactory.getProbe.apply(x);
        return probe.getX() >= 0 && probe.getX() <= plateau.getMaxX() && probe.getY() >= 0 && probe.getY() <= plateau.getMaxY();
    };
}
