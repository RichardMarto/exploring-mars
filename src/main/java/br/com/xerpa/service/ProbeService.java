package br.com.xerpa.service;

import br.com.xerpa.factory.ProbeFactory;
import br.com.xerpa.model.Probe;
import br.com.xerpa.rotation.Directions;
import br.com.xerpa.helper.ProbeHelper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProbeService {

    private static final Logger logger = Logger.getLogger( ProbeService.class.getName() );

    private PlateauService plateau;
    private final ProbeFactory probeFactory = new ProbeFactory();
    private final ProbeHelper probeHelper = new ProbeHelper();

    public ProbeService (PlateauService plateauService) {
        this.plateau = plateauService;
    }

    public String processData (String position, String commands) {
        if (plateau.contains.and(probeHelper.isValid).test(position)) {
            return processCommands
                    .andThen(stringfy)
                    .apply(probeFactory.getProbe.apply(position), probeHelper.clean.apply(commands));
        } else {
            String msg = String.format("%s is not a valid input, please check the documentation and use a valid input format!", commands);
            logger.log(Level.SEVERE, msg);
            return "Error with a probe original position!";
        }
    }

    private final Function<Probe, String> stringfy = probe -> {
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.asList(probe.getX(), " ", probe.getY(), " ", probe.getDirection()).forEach(stringBuilder::append);
        return stringBuilder.toString();
    };

    private final BiFunction<Probe, Integer, Probe> processRotation = (probe, clockWiseRotations) -> {
        while (clockWiseRotations != 0) {
            if (clockWiseRotations > 0) {
                probe.rotateRight();
                clockWiseRotations--;
            } else {
                probe.rotateLeft();
                clockWiseRotations++;
            }
        }
        return probe;
    };

    private final ToIntFunction<String> getClockWiseRotations = command -> {
        Map<Character, Integer> wordCount = probeHelper.wordCounter(command);
        return  wordCount.get(Directions.RIGHT) - wordCount.get(Directions.LEFT);
    };

    private final Consumer<Probe> turnBack = probe -> {
        probe.rotateRight();
        probe.rotateRight();
    };

    private final Consumer<Probe> undoMove = turnBack.andThen(Probe::move).andThen(turnBack);

    private final BiFunction<Probe, Boolean, Probe> moveIfNeeded = (probe, needToMove) -> {
        if (needToMove) {
            probe.move();
        }
        if (plateau.have.negate().test(probe.getX(), probe.getY())) {
            logger.log(Level.WARNING, "A probe cannot move to outside panned plateau!");
            undoMove.accept(probe);
        }
        return probe;
    };

    private final Function<String, UnaryOperator<Probe>> processCommand = command -> {
        Integer clockWiseRotations = getClockWiseRotations.applyAsInt(command);
        UnaryOperator<Probe> processMovement = p -> moveIfNeeded.apply(p, command.contains("M"));
        return probe -> processRotation.andThen(processMovement).apply(probe, clockWiseRotations);
    };

    private final UnaryOperator<String> addSeparator = commands -> commands.replace("M", "M ");

    private final Function<String, String[]> split = commands -> commands.split(" ");

    private final Function<String, List<String>> getInstructions = addSeparator.andThen(split).andThen(Arrays::asList);

    private final BiFunction<Probe, String, Probe> processCommands = (probe, commands) -> {
        getInstructions.apply(commands).forEach(command -> processCommand.apply(command).apply(probe));
        return probe;
    };
}
