package br.com.xerpa.movement;

import br.com.xerpa.model.Probe;

import java.util.function.Consumer;

public class Movement {
    private Movement() {}

    public static final Consumer<Probe> getMovement = Movement::getMovement;

    private static void getMovement (Probe probe) {
        switch(probe.getDirection()) {
            case 'N':
                Movement.moveNorth(probe);
                break;
            case 'E':
                Movement.moveEast(probe);
                break;
            case 'S':
                Movement.moveSouth(probe);
                break;
            case 'W':
                Movement.moveWest(probe);
                break;
            default:
                break;
        }
    }

    private static void moveNorth (Probe probe) {
        probe.setY(probe.getY() + 1);
    }

    private static void moveEast (Probe probe) {
        probe.setX(probe.getX() + 1);
    }

    private static void moveSouth (Probe probe) {
        probe.setY(probe.getY() - 1);
    }

    private static void moveWest (Probe probe) {
        probe.setX(probe.getX() - 1);
    }
}
