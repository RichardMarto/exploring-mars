package br.com.xerpa.model;

import br.com.xerpa.movement.Movement;
import br.com.xerpa.rotation.Rotation;

import java.util.function.Consumer;

public class Probe implements SpaceCraft{
    private Character direction;
    private Long x;
    private Long y;

    public Probe(Character direction, Long x, Long y) {
        this.direction = direction;
        this.x = x;
        this.y = y;
    }

    @Override
    public void rotateRight() {
        Rotation.getRightRotation(direction).ifPresent(this::rotate);
    }

    @Override
    public void rotateLeft() {
        Rotation.getLeftRotation(direction).ifPresent(this::rotate);
    }

    private void rotate(Consumer<Probe> rotation) {
        rotation.accept(this);
    }

    @Override
    public void move() {
        Movement.getMovement.accept(this);
    }

    public Character getDirection() {
        return direction;
    }

    public void setDirection(Character direction) {
        this.direction = direction;
    }

    public Long getX() {
        return x;
    }

    public void setX(Long x) {
        this.x = x;
    }

    public Long getY() {
        return y;
    }

    public void setY(Long y) {
        this.y = y;
    }
}
