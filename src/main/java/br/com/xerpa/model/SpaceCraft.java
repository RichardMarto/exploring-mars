package br.com.xerpa.model;

public interface SpaceCraft {
    public void rotateRight() ;
    public void rotateLeft();
    public void move();
}
