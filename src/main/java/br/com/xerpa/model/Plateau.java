package br.com.xerpa.model;

public class Plateau {
    public Plateau(Long maxX, Long maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
    }

    private final Long maxX;
    private final Long maxY;

    public Long getMaxX() {
        return maxX;
    }

    public Long getMaxY() {
        return maxY;
    }
}
