package br.com.xerpa.factory;

import br.com.xerpa.model.Plateau;

public class PlateauFactory implements BaseFactory{

    public Plateau getPlateau(String maxPoint) {
        return new Plateau(getLong.apply(maxPoint, 0), getLong.apply(maxPoint, 1));
    }
}
