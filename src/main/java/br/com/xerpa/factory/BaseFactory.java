package br.com.xerpa.factory;

import java.util.function.BiFunction;
import java.util.function.ToLongFunction;

public interface BaseFactory {

    ToLongFunction<String> toLong = string -> Long.parseLong(string);

    BiFunction<String, Integer, Long> getLong = (string, index) -> toLong.applyAsLong(string.split("\\s")[index]);
}
