package br.com.xerpa.factory;

import br.com.xerpa.model.Probe;

import java.util.function.Function;

public class ProbeFactory implements BaseFactory{

    public final Function<String, Probe> getProbe = position -> {
        char direction = position.toCharArray().length > 4 ? position.toCharArray()[4] : 'N';
        return new Probe(direction, getLong.apply(position, 0), getLong.apply(position, 1));
    };
}
