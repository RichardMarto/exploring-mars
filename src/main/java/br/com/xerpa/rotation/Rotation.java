package br.com.xerpa.rotation;

import br.com.xerpa.model.Probe;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rotation {

    private Rotation() {}

    private static final Logger logger = Logger.getLogger( Rotation.class.getName() );

    private static final String errorMsg = "{0} is not a valid direction, please check the documentation and use a valid input format!";

    private static final Consumer<Character> logError = direction -> logger.log(Level.SEVERE, errorMsg, direction);

    public static Optional<Consumer<Probe>> getRightRotation(Character currentDirection) {
        switch(currentDirection) {
            case 'N':
                return getRotation(Directions.EAST);
            case 'E':
                return getRotation(Directions.SOUTH);
            case 'S':
                return getRotation(Directions.WEST);
            case 'W':
                return getRotation(Directions.NORTH);
            default:
                logError.accept(currentDirection);
                return Optional.empty();
        }
    }

    public static Optional<Consumer<Probe>> getLeftRotation(Character currentDirection) {
        switch(currentDirection) {
            case 'N':
                return getRotation(Directions.WEST);
            case 'E':
                return getRotation(Directions.NORTH);
            case 'S':
                return getRotation(Directions.EAST);
            case 'W':
                return getRotation(Directions.SOUTH);
            default:
                logError.accept(currentDirection);
                return Optional.empty();
        }
    }

    private static Optional<Consumer<Probe>> getRotation (Character direction) {
        Consumer<Probe> rotate = probe -> probe.setDirection(direction);
        return Optional.of(rotate);
    }
}
