package br.com.xerpa.rotation;

public class Directions {
    public static final Character NORTH = 'N';
    public static final Character EAST = 'E';
    public static final Character SOUTH = 'S';
    public static final Character WEST = 'W';
    public static final char RIGHT = 'R';
    public static final char LEFT = 'L';
}
