package br.com.xerpa;

import br.com.xerpa.service.PlateauService;
import br.com.xerpa.service.ProbeService;

import java.util.Scanner;

class Runner {
    private final Scanner scanner = new Scanner(System.in);
    private final String maxPoint = scanner.nextLine();
    private final PlateauService plateauService = new PlateauService(maxPoint);
    private final ProbeService probeService = new ProbeService(plateauService);

    void run () {
        StringBuilder stringBuilder = new StringBuilder();
        while(scanner.hasNext()){
            String originalPosition = scanner.nextLine();
            if (scanner.hasNext()) {
                String commands = scanner.nextLine();
                stringBuilder.append("\n");
                stringBuilder.append(probeService.processData(originalPosition, commands));
            }
            System.out.println(stringBuilder.toString());
            System.out.println("\n");
        }
    }
}
