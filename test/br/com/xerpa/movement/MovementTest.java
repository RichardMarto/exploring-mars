package br.com.xerpa.movement;

import br.com.xerpa.model.Probe;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import br.com.xerpa.rotation.Directions;

@RunWith(JUnit4.class)
public class MovementTest {

    @Test
    public void moveNorth(){
        final Probe probe = new Probe(Directions.NORTH, 0L, 0L);
        probe.move();
        Assert.assertEquals(Long.valueOf(0), probe.getX());
        Assert.assertEquals(Long.valueOf(1), probe.getY());
    }

    @Test
    public void moveEast(){
        final Probe probe = new Probe(Directions.EAST, 0L, 0L);
        probe.move();
        Assert.assertEquals(Long.valueOf(1), probe.getX());
        Assert.assertEquals(Long.valueOf(0), probe.getY());
    }

    @Test
    public void moveSouth(){
        final Probe probe = new Probe(Directions.SOUTH, 0L, 0L);
        probe.move();
        Assert.assertEquals(Long.valueOf(0), probe.getX());
        Assert.assertEquals(Long.valueOf(-1), probe.getY());
    }

    @Test
    public void moveWest(){
        final Probe probe = new Probe(Directions.WEST, 0L, 0L);
        probe.move();
        Assert.assertEquals(Long.valueOf(-1), probe.getX());
        Assert.assertEquals(Long.valueOf(0), probe.getY());
    }
}