package br.com.xerpa.rotation;

import br.com.xerpa.model.Probe;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RotationTest {

    @Test
    public void rotateRight() {
        Probe probe = new Probe(Directions.NORTH, 0L, 0L);
        Assert.assertEquals(probe.getDirection(), Directions.NORTH);
        probe.rotateRight();
        Assert.assertEquals(probe.getDirection(), Directions.EAST);
        probe.rotateRight();
        Assert.assertEquals(probe.getDirection(), Directions.SOUTH);
        probe.rotateRight();
        Assert.assertEquals(probe.getDirection(), Directions.WEST);
        probe.rotateRight();
        Assert.assertEquals(probe.getDirection(), Directions.NORTH);

        Assert.assertFalse(Rotation.getRightRotation('A').isPresent());
    }

    @Test
    public void rotateLeft() {
        Probe probe = new Probe(Directions.NORTH, 0L, 0L);
        Assert.assertEquals(probe.getDirection(), Directions.NORTH);
        probe.rotateLeft();
        Assert.assertEquals(probe.getDirection(), Directions.WEST);
        probe.rotateLeft();
        Assert.assertEquals(probe.getDirection(), Directions.SOUTH);
        probe.rotateLeft();
        Assert.assertEquals(probe.getDirection(), Directions.EAST);
        probe.rotateLeft();
        Assert.assertEquals(probe.getDirection(), Directions.NORTH);

        Assert.assertFalse(Rotation.getLeftRotation('A').isPresent());
    }
}