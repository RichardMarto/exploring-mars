package br.com.xerpa.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ProbeServiceTest {

    private final PlateauService plateauService = new PlateauService("10 10");
    private final ProbeService probeService = new ProbeService(plateauService);

    @Test
    public void processData() {
        Assert.assertEquals("1 3 N", probeService.processData("1 2 N", "LM LMLML  MM"));
        Assert.assertEquals("5 1 E", probeService.processData("3 3 E", "MMR MMR MRRM"));
        Assert.assertEquals("4 1 E", probeService.processData("3 3 E", "MM RM MRMR234R"));
    }

    @Test
    public void processDataError() {
        String error = "Error with a probe original position!";
        Assert.assertEquals(error, probeService.processData("1 2  N", "LM LMLML   MM"));
        Assert.assertEquals(error, probeService.processData("1 2 3 N", "LM LMLML  MM"));
        Assert.assertEquals(error, probeService.processData("1 2 3", "LM LMLML  MM"));
    }

    @Test
    public void probeMovinggOutsidThePlateau() {
        Assert.assertEquals("0 0 S", probeService.processData("0 0 S", "M"));
    }
}