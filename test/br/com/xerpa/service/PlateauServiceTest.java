package br.com.xerpa.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PlateauServiceTest {
    PlateauService plateauService = new PlateauService("5 5");

    @Test
    public void have () {
        Assert.assertTrue(plateauService.contains.test("0 0"));
        Assert.assertTrue(plateauService.contains.test("1 2"));
        Assert.assertFalse(plateauService.contains.test("0 6"));
    }

    @Test
    public void contains () {
        Assert.assertTrue(plateauService.have.test(0L, 0L));
        Assert.assertTrue(plateauService.have.test(1L, 2L));
        Assert.assertFalse(plateauService.have.test(0L, 6L));
        Assert.assertFalse(plateauService.have.test(-1L, 2L));
    }
}