# exploring-mars
Xerpa's back-end code challenge

## Dependencies
- You will need Java 8 and Maven to run this project.  

## Build
- You can install the program with the following command:  
`mvn clean install`  

## Run
- To run the program you can use the following command:  
`java -jar target/exploring-mars-1.0-SNAPSHOT.jar`  

## Use guide

- The first input line should be the upper right point of the plateau:  
`5 5`  

- After inserting the upper right point, you can enter the probe position and the commands for it:  
LMLMLMLMM  
`3 3 E`  

- The final position of the probes will be printed:  
`1 3 N`  

- Then you can repeat the processe of inserting the data for a new probe:  
`3 3 E`  
`MMRMMRMRRM`  

- The final position of all probes will be printed again:  
`1 3 N`  
`5 1 E`  

- Be aware that typo may crash the program.

More information can be found on https://docs.google.com/document/d/1G4oMTsvZOP61lXmdQXFIPiy25GjCt8LvVDYJiItCS-U/edit#.
